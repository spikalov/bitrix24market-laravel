<?php

use GuzzleHttp\Psr7\Header;
use Illuminate\Support\Facades\Route;
use Vipblogger\LaravelBitrix24\Bitrix;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/', 'App\Http\Controllers\MainController@main');
Route::any('/install', 'App\Http\Controllers\InstallController@install');
