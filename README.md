# Каркас приложения на Laravel для маркета Битрикс24

В .env добавить 
```
B24_APPLICATION_ID=local.6007b39598c****.120****
B24_APPLICATION_SECRET=oq33VjMcuperGAnB...
B24_DOMAIN=example.bitrix24.ru
B24_APPLICATION_SCOPE=task,user
```
Где: 

**B24_APPLICATION_ID** - client id/app id

**B24_APPLICATION_SECRET** - client secret/ app secret

**B24_APPLICATION_SCOPE** - Права приложения

***

В БД создать таблицу
```
CREATE TABLE portals (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    access_token VARCHAR(255) NOT NULL,
    refresh_token VARCHAR(255) NOT NULL,
    member_id VARCHAR(255) NOT NULL
);
```
***
Основная ссылка https://site.ru/

Ссылка на установку https://site.ru/install
