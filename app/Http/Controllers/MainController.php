<?php

namespace App\Http\Controllers;

use Bitrix24\Bitrix24;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Vipblogger\LaravelBitrix24\Bitrix;

class MainController extends Controller
{
    public function main(Request $request, Bitrix $bitrix)
    {
        $domain = $request->input('DOMAIN');

        self::auth($domain, $bitrix);

        $b24 = new \Bitrix24\CRM\Deal\Deal($bitrix);
        $deals = $b24->getList();

        echo '<pre>';
        print_r($deals);
        echo '</pre>';

        self::refreshToken($domain, $bitrix);
    }

    /**
     * Авторизация с помощью OAuth 2.0
     *
     * @param String $domain Домен портала
     * @param Bitrix $bitrix Объект класса bitrix
     * @return void
     */
    public static function auth(String $domain, Bitrix $bitrix)
    {
        $portals = DB::select("SELECT * FROM portals WHERE domain = '$domain' LIMIT 1");
        $row = $portals[0];

        $bitrix->setDomain($domain);
        $bitrix->setAccessToken($row->access_token);
        $bitrix->setRefreshToken($row->refresh_token);
        $bitrix->setMemberId($row->member_id);
    }

    /**
     * Обновление токенов
     *
     * @param String $domain Домен портала
     * @param Bitrix $bitrix Объект класса bitrix
     * @return void
     */
    public static function refreshToken(String $domain, Bitrix $bitrix)
    {
        $newAccess = $bitrix->getNewAccessToken();

        if (!empty($newAccess['access_token']) && !empty($newAccess['refresh_token']) && !empty($domain)) {
            $access = $newAccess['access_token'];
            $refresh = $newAccess['refresh_token'];

            $bitrix->setAccessToken($access);
            $bitrix->setRefreshToken($refresh);

            DB::update("UPDATE portals
            SET access_token = '$access', refresh_token = '$refresh'
            WHERE domain = '$domain'");
        }
    }
}
