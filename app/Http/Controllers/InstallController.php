<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InstallController extends Controller
{
    public function install(Request $request)
    {
        $domain = $request->input('DOMAIN');
        $access = $request->input('AUTH_ID');
        $refresh = $request->input('REFRESH_ID');
        $memberID = $request->input('member_id');

        $portals = DB::select("SELECT * FROM portals WHERE domain = '$domain' LIMIT 1");

        if (empty($portals)) {
            DB::insert("INSERT INTO portals (domain, access_token, refresh_token, member_id)
            VALUES ('$domain', '$access', '$refresh', '$memberID')");
        } else {
            DB::update("UPDATE portals
            SET access_token = '$access', refresh_token = '$refresh', member_id = '$memberID'
            WHERE domain = '$domain'");
        }

        return '<script src="//api.bitrix24.com/api/v1/"></script>
                <script>BX24.init(function() {
                    BX24.installFinish();
                });</script>';
    }
}
